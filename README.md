# self-replicating-programs
## A number of self replicating programs

* `q2.php` - PHP
* `self_replicating.py` - Python 2.x
* `self_rep_alternating.py` - Python 2.x, alternates generations
* `replicate.html` - JavaScript and HTML, replicates in a browser. [Try it!](https://rawgit.com/bediger4000/self-replicating-programs/master/replicate.html)
* `triplet` - Perl, Bash, Python 2.x and PHP quine relay/ouroboros program. Execute `run_replication` to try it out.
*  `replicate` - shell script, Bash, probably ksh. Changes just a little with each reproduction.
